# goagen Image

[![pipeline status](https://gitlab.com/fixl/docker-goagen/badges/master/pipeline.svg)](https://gitlab.com/fixl/docker-goagen/-/pipelines)
[![version](https://fixl.gitlab.io/docker-goagen/version.svg)](https://gitlab.com/fixl/docker-goagen/-/commits/master)
[![size](https://fixl.gitlab.io/docker-goagen/size.svg)](https://gitlab.com/fixl/docker-goagen/-/commits/master)
[![Docker Pulls](https://img.shields.io/docker/pulls/fixl/goagen)](https://hub.docker.com/r/fixl/goagen)
[![Docker Stars](https://img.shields.io/docker/stars/fixl/goagen)](https://hub.docker.com/r/fixl/goagen)

A Docker container containing [goagen](https://github.com/goadesign/goa#code-generation), a code
generator for [Goa](https://goa.design/), a framework for building micro-services and APIs in Go
using a unique design-first approach.

This image can be used with [3 Musketeers](https://3musketeers.io/).

## Build the image

```bash
make build
```

## Inspect the image

```bash
docker inspect --format='{{ range $k, $v := .Config.Labels }}{{ printf "%s=%s\n" $k $v}}{{ end }}' fixl/goagen:latest
```

## Usage

```bash
docker run --rm -it fixl/goagen

docker run --rm -it -v $(pwd)/src fixl/goagen gen goa.design/examples/basic/design -o gen
```
