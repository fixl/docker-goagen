FROM golang:alpine as builder

ARG GOA_VERSION

RUN go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest \
        && go install google.golang.org/protobuf/cmd/protoc-gen-go@latest \
        && go install goa.design/goa/v3/...@v${GOA_VERSION}

FROM golang:alpine

ARG GOA_VERSION
ENV GOA_VERSION ${GOA_VERSION}

RUN apk add --no-cache --update \
    protoc \
    make \
    bash \
    git

COPY --from=builder /go/bin/goa /go/bin/goa
COPY --from=builder /go/bin/protoc-gen-go /go/bin/protoc-gen-go
COPY --from=builder /go/bin/protoc-gen-go-grpc /go/bin/protoc-gen-go-grpc

# Set GOCACHE to /tmp to avoid issues when run with different user and/or group id
ENV GOCACHE /tmp

WORKDIR "/src"

ENTRYPOINT ["goa"]
